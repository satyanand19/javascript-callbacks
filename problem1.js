let fs = require('fs');

function createFolder(path,callback,callbackToDelete) {
    fs.mkdir(path, (error) => {
        if (error) {
            console.error(error);
        } else {
            console.log("Folder Created");
            for(let index = 0;index < 3 ;index++) {
                callback(`Data of Json Files/data${index}.json`);
            }
            // callback('Data/bag.json'); // folder does not exit
            fs.readdir(path,(error ,files) => {
                if(error) {
                    console.log('folder does not exits');
                }
                else if(files.length === 3) {
                    callbackToDelete('Data of Json Files');
                }

            })
            
        }
    });
}
function createFile(path) {
        fs.writeFile(path, '', (error) => {
            if (error) {
                console.error(error);
            } else {
                console.log("File Created");
            }
        });
}
function deleteAllFile(path) {
    fs.readdir(path, (error, filesName) => {
        if (error) {
            console.error(error);
        } else {
            filesName.forEach((item) => {
                item = path + '/' + item;
                fs.unlink(item, (err) => {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log(`${item} File are deleted`);
                    }
                });
            });
        }
    });
}

module.exports = {
    createFile,
    createFolder,
    deleteAllFile
};
