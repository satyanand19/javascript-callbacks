let fs = require('fs');
let pathToConnect = require('path');

function writeFile(path, data) {
    fs.appendFile(path, data, (err) => {
        if (err) {
            console.error(err);
        } else {
            console.log("file created and written the data into it");
        }
    });
}

function deleteAllNewFiles(path) {
    fs.readFile(path, 'utf-8', (err, data) => {
        if (err) {
            console.log(err);
        } else {
            let fileArray = data.split(' ');
            fileArray.pop();
            fileArray.forEach((item) => {
                fs.unlink(item, (err) => {
                    if (err) {
                        console.error(err);
                    }
                    else {
                        console.log("File Deleted");
                    }
                });
            });

        }
    });
}

function sortAndStore(path, newPath,callback) {
    fs.readFile(path, 'utf-8', (err, data) => {
        if (err) {
            console.log(err);
        } else {
            let array = JSON.parse(data);
            array.sort((a, b) => {
                return a > b ? 1 : -1;
            });
            writeFile(newPath, JSON.stringify(array));
            writeFile('filenames.txt', newPath + ' ');
            callback(pathToConnect.join(__dirname, './filenames.txt'));
        }
    });
}

function convertIntoLowerCaseAndStore(path, newPath,callback) {
    fs.readFile(path, 'utf-8', (err, data) => {
        if (err) {
            console.log(err);
        } else {
            let sentence = JSON.stringify(data.toLowerCase().split('.'));
            writeFile(newPath, sentence);
            writeFile('filenames.txt', newPath + ' ');
            callback(pathToConnect.join(__dirname, './LowerCaseInSentence.txt'), 'sortedData.txt',deleteAllNewFiles);

        }
    });
}

function convertIntoUpperCase(path, data,callback) {
    data = data.toUpperCase();
    writeFile(path, data);
    writeFile('filenames.txt', path + ' ');
    callback(pathToConnect.join(__dirname, './upperCase.txt'), 'LowerCaseInSentence.txt',sortAndStore);

}

function readFileData(path, newPath, callback) {
    fs.readFile(path, 'utf-8', (err, data) => {
        if (err) {
            console.log(err);
        } else {
            callback(newPath, data,convertIntoLowerCaseAndStore);
        }
    });

}



module.exports = {
    readFileData,
    convertIntoUpperCase,
}